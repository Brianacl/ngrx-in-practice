import * as RecipesAcitons from './recipe.actions';

import { Recipe } from '../recipe.model';

export interface State {
    recipes: Recipe[];
}

const initialState: State = {
    recipes: []
}

export function recipeReducer(state = initialState, action: RecipesAcitons.RecipesAcitons) {
    switch(action.type) {
        case RecipesAcitons.SET_RECIPES:
            return {
                ...state,
                recipes: [...action.payload]
            };
        case RecipesAcitons.ADD_RECIPE:
            return {
                ...state,
                recipes: [...state.recipes, action.payload]
            }
        case RecipesAcitons.UPDATE_RECIPE:
            const updatedRecipe = {
                ...state.recipes[action.payload.index],
                ...action.payload.newRecipe
            };

            const updatedRecipes = [...state.recipes]
            updatedRecipes[action.payload.index] = updatedRecipe;

            return {
                ...state,
                recipes: updatedRecipes
            };
        case RecipesAcitons.DELETE_RECIPE:
            return {
                ...state,
                recipes: state.recipes.filter((recipe, i) => i !== action.payload)
            };
        default:
            return state;
    }
}