import { Action } from '@ngrx/store';

export const SIGNUP_START = '[Auth] SIGNUP START';
export const LOGIN_START = '[Auth] LOGIN START';
export const AUTO_LOGIN = '[Auth] AUTO LOGIN';

export const AUTHENTICATE_SUCCESS = '[Auth] AUTHENTICATE';
export const AUTHENTICATE_FAIL = '[Auth] AUTHENTICATE FAIL';

export const SIGNUP = '[Auth] SIGNUP';

export const LOGOUT = '[Auth] LOGOUT';
export const AUTO_LOGOUT = '[Auth] AUTO LOGOUT'

export const CLEAR_ERROR = '[Auth] CLEAR ERROR';

export class AuthenticateSuccess implements Action {
    readonly type = AUTHENTICATE_SUCCESS;

    constructor(public payload: {
        email: string,
        userId: string,
        token: string,
        expirationDate: Date,
        redirect: Boolean
    }) { }
}

export class LoginStart implements Action {
    readonly type = LOGIN_START;
    constructor(public payload: { email: string, password: string }) {}
}

export class SignupStart implements Action {
    readonly type = SIGNUP_START;
    constructor(public payload: { email: string, password: string }) {}
}

export class AuthenticateFail implements Action {
    readonly type = AUTHENTICATE_FAIL;
    constructor(public payload: string) { }
}

export class ClearError implements Action {
    readonly type = CLEAR_ERROR;
}

export class AutoLogin implements Action {
    readonly type = AUTO_LOGIN;
}

export class Logout implements Action {
    readonly type = LOGOUT;
}

export type AuthActions = 
    | AuthenticateSuccess
    | Logout
    | LoginStart
    | SignupStart
    | AuthenticateFail
    | ClearError
    | AutoLogin;